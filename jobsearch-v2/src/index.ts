import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { JobclientModelModule } from '@jobclient/model';
import { JobclientStoreModule } from '@jobclient/store';

import { SampleComponent } from './sample.component';
import { SampleDirective } from './sample.directive';
import { SamplePipe } from './sample.pipe';
import { SampleService } from './sample.service';

export * from './sample.component';
export * from './sample.directive';
export * from './sample.pipe';
export * from './sample.service';

@NgModule({
  imports: [
    CommonModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    JobclientModelModule.forRoot(),
    JobclientStoreModule.forRoot()
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class JobclientSearchModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: JobclientSearchModule,
      providers: [SampleService]
    };
  }
}
